variable "app_name" {
  description = "Project Name"
  default = "blacklacost-store"
}

variable "buildpacks" {
  description = "Heroku buildpacks"
  type = list
  default = [
    "heroku/nodejs",
    "https://github.com/heroku/heroku-buildpack-python",
  ]
}

terraform {
  backend "remote" {
    organization = "blacklacost"
    workspaces {
      name = "heroku-blacklacost-store"
    }
  }
}

provider "heroku" {}


module "app-stage" {
  source = "./modules/stage"

  app_name = var.app_name
  buildpacks = var.buildpacks
}

module "app-prod" {
  source = "./modules/prod"

  app_name = var.app_name
  buildpacks = var.buildpacks
}
