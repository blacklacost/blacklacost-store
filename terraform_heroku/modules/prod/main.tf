variable "app_name" {}
variable "buildpacks" {}

module "app-prod" {
  source = "../app"

  app_name = var.app_name
  app_type = "prod"
  buildpacks = var.buildpacks
}
