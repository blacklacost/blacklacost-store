variable "app_name" {}
variable "buildpacks" {}

module "app-stage" {
  source = "../app"

  app_name = var.app_name
  app_type = "stage"
  buildpacks = var.buildpacks
}
