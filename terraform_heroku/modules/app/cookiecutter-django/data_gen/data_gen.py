import os
import secrets
import sys
import json


def admin_url_gen():
    return f"{secrets.token_urlsafe(32)[:32]}/"


def secret_key_gen():
    return secrets.token_urlsafe(64)[:64] 


def main():
    result = {
        "admin_url": admin_url_gen(),
        "secret_key": secret_key_gen(),
    }
    sys.stdout.write(json.dumps(result))


if __name__ == '__main__':
    main()