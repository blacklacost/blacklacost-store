variable "app_name" {}
variable "addon_plan" {}

module "addon" {
  source = "../addon"

  app_name = var.app_name
  addon_plan = var.addon_plan
  addon_name = "${var.app_name}-redis"
}

data "heroku_app" "app" {
  name = var.app_name
  depends_on = [
    module.addon
  ]
}

output "redis_url" {
  value = data.heroku_app.app.config_vars.REDIS_URL
}